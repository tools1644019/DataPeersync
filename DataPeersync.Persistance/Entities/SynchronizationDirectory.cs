﻿using SQLite;

namespace DataPeersync.Persistance.Entities;

internal sealed class SynchronizationDirectory
{
	[PrimaryKey]
	public int Id { get; set; }

	public string? Path { get; set; }
}
