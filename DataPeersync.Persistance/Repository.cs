﻿using DataPeersync.Persistance.Entities;
using SQLite;

namespace DataPeersync.Persistance;

public class Repository(string databaseDirectoryPath)
{
	public async Task<string?> GetSyncronizationDirectoryPathAsync()
	{
		await database.CreateTableAsync<SynchronizationDirectory>();
		return (await database!.Table<SynchronizationDirectory>()
			.FirstOrDefaultAsync())
			?.Path;
	}

	public async Task SetSyncronizationDirectoryPathAsync(string value)
	{
		await database.CreateTableAsync<SynchronizationDirectory>();
		await database!.InsertOrReplaceAsync(new SynchronizationDirectory { Id = 1, Path = value });
	}

	private readonly SQLiteAsyncConnection database = new(
		databasePath: Path.Combine(databaseDirectoryPath, filename),
		flags);

	private const SQLiteOpenFlags flags =
		SQLiteOpenFlags.ReadWrite |
		SQLiteOpenFlags.Create |
		SQLiteOpenFlags.SharedCache; // multi-threaded database access

	private const string filename = "DataPeersync.Persistance.db3";
}
