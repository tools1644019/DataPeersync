﻿namespace DataPeersync.Transfer;

public interface ILogger
{
	void Log(string message);
}
