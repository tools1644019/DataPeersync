﻿namespace DataPeersync.Transfer;

internal static class ByteArrayExtensions
{
	public static byte[] Concat(this byte[] @this, byte[] other)
	{
		var combinedArray = new byte[@this.Length + other.Length];
		Buffer.BlockCopy(src: @this, srcOffset: 0, dst: combinedArray, dstOffset: 0, count: @this.Length);
		Buffer.BlockCopy(src: other, srcOffset: 0, dst: combinedArray, dstOffset: @this.Length, count: other.Length);
		return combinedArray;
	}

	public static bool StartsWith(this byte[] @this, byte[] prefix)
	{
		return @this.Length >= prefix.Length
			&& @this[..prefix.Length].SequenceEqual(prefix);
	}

	public static bool EndsWith(this byte[] @this, byte[] suffix)
	{
		return @this.Length >= suffix.Length
			&& @this[^suffix.Length..].SequenceEqual(suffix);
	}
}
