﻿namespace DataPeersync.Transfer.Exceptions;

public class InsufficientDataReceivedException(int receivedBytesNumber, long expectedBytesNumber)
	: Exception($"Read {receivedBytesNumber} bytes but there are {expectedBytesNumber} bytes expected.")
{ }
