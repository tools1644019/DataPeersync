﻿namespace DataPeersync.Transfer.Exceptions;

public class UnexpectedDataReceivedException(int bytesNumber)
	: Exception($"Received more bytes than expected. Exceeded bytes number: {bytesNumber}")
{ }
