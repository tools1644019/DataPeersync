﻿namespace DataPeersync.Transfer;

internal enum TransferMarker
{
	Start,
	End,
	Abort,

	Int,
	Long,
	String,

	FileStart,
	FileEnd,
	FileAbort,
}
