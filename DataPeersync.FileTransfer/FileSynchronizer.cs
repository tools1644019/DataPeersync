﻿using System.Net;
using System.Net.Sockets;

namespace DataPeersync.Transfer;

public sealed class FileSynchronizer(ILogger logger)
{
	public async Task SynchronizeAsync(
		string directoryPath,
		IPAddress ipAddress,
		CancellationToken cancellationToken)
	{
		var gettingStreamCancellationSource = new CancellationTokenSource();
		var combinedCancellationToken = Combine(gettingStreamCancellationSource, cancellationToken);

		using var tcpListener = new TcpListener(IPAddress.Any, Port);
		tcpListener.Start(backlog: 1);
		var waitingForIncomingConnection = tcpListener.AcceptTcpClientAsync(combinedCancellationToken)
			.AsTask();

		using var initiativeTcpClient = new TcpClient();
		var connectingToOtherDevice = initiativeTcpClient.ConnectAsync(ipAddress, Port, combinedCancellationToken)
			.AsTask();

		await Task.WhenAny(waitingForIncomingConnection, connectingToOtherDevice);
		gettingStreamCancellationSource.Cancel();

		if (waitingForIncomingConnection.IsCompleted)
			logger.Log("Incoming connection accepted.");
		else
			logger.Log("Connected to the other device.");

		using var networkStream = waitingForIncomingConnection.IsCompleted
			? waitingForIncomingConnection.Result.GetStream()
			: initiativeTcpClient.GetStream();

		var fileReceivingTask = new FileReceiver(networkStream, directoryPath, logger, cancellationToken)
			.ReceiveMultipleAsync();

		var fileSendingTask = new FileSender(networkStream, directoryPath, logger, cancellationToken)
			.SendChildrenAsync();

		await Task.WhenAll(fileReceivingTask, fileSendingTask);
	}

	private static CancellationToken Combine(
		CancellationTokenSource cancellationSource,
		CancellationToken cancellationToken)
	{
		return CancellationTokenSource
			.CreateLinkedTokenSource(cancellationToken, cancellationSource.Token)
			.Token;
	}

	private const int Port = 51333;
}
