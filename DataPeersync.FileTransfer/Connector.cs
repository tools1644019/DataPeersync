﻿using System.Net;
using System.Net.Sockets;
using System.Text;

namespace DataPeersync.Transfer;

public sealed class Connector(ILogger logger)
{
	public async Task BroadcastPresenceAsync(CancellationToken cancellationToken)
	{
		logger.Log("Broadcasting presence...");

		using var client = new UdpClient { EnableBroadcast = true };
		var endpoint = new IPEndPoint(IPAddress.Broadcast, broadcastPort);
		var broadcastPhrase = broadcastPhrasePrefix.Concat(thisDeviceGuidBytes);

		while (!cancellationToken.IsCancellationRequested)
		{
			await client.SendAsync(broadcastPhrase, endpoint, cancellationToken);
			await Task.Delay(millisecondsDelay: 5000, cancellationToken);
		}

		logger.Log("Broadcasting cancelled.");
	}

	public async Task<IPAddress> GetOtherDeviceIpAsync(CancellationToken cancellationToken)
	{
		logger.Log("Getting recipient IP...");

		using var listener = new UdpClient(broadcastPort);

		while (!cancellationToken.IsCancellationRequested)
		{
			var result = await listener.ReceiveAsync(cancellationToken);

			if (result.Buffer.StartsWith(broadcastPhrasePrefix)
				&& !result.Buffer.EndsWith(thisDeviceGuidBytes))
			{
				logger.Log($"Recipient IP received: {result.RemoteEndPoint.Address}.");
				return result.RemoteEndPoint.Address;
			}
		}

		throw new OperationCanceledException();
	}

	private readonly byte[] thisDeviceGuidBytes = Guid.NewGuid().ToByteArray();
	private readonly byte[] broadcastPhrasePrefix = Encoding.UTF8.GetBytes("DataPeersync");

	private const int broadcastPort = 9878;
	private const int guidLengthInBytes = 16;
}
