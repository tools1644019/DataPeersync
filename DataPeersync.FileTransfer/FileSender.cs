using System.Net.Sockets;

namespace DataPeersync.Transfer;

internal sealed class FileSender(
	NetworkStream networkStream,
	string directoryPath,
	ILogger logger,
	CancellationToken cancellationToken)
{
	public async Task<List<string>> SendChildrenAsync()
	{
		logger.Log("Receiving files...");

		try
		{
			await SendAsync(TransferMarker.Start);
			await SendAsync(filePaths.Count, "Files count");

			foreach (var filePath in filePaths)
			{
				var relativeFilePath = Path.GetRelativePath(relativeTo: directoryPath, path: filePath);
				await SendAsync(relativeFilePath, "File name");

				using var file = new FileStream(filePath, FileMode.Open);
				logger.Log($"Sending file \"{filePath}\".");
				await SendAsync(file);
			}

			logger.Log("All files sent.");

			await SendAsync(TransferMarker.End);

			return filePaths;
		}
		catch (Exception ex)
		{
			logger.Log($"Error: {ex.Message}");
			await SendAsync(TransferMarker.Abort);
			throw;
		}
	}

	private async Task SendAsync(Stream file)
	{
		await SendAsync(TransferMarker.FileStart);
		await SendAsync(file.Length, "File length");

		try
		{
			var buffer = new byte[Configuration.ChunkSize].AsMemory();
			var bytesToSendNumber = file.Length;

			do
			{
				var readBytesNumber = await file.ReadAsync(buffer, cancellationToken);

				if (readBytesNumber == 0)
					throw new InvalidOperationException(
						$"Error in reading the file. Read {readBytesNumber} bytes but there are {bytesToSendNumber} bytes left to send.");

				await networkStream.WriteAsync(buffer[..readBytesNumber], cancellationToken);
				bytesToSendNumber -= readBytesNumber;
			}
			while (bytesToSendNumber > 0);

			if (bytesToSendNumber < 0)
			{
				throw new InvalidOperationException(
					$"Sent more bytes than file length ({-bytesToSendNumber} bytes exceeds)");
			}
		}
		catch (Exception ex)
		{
			logger.Log($"Error: {ex.Message}");
			await SendAsync(TransferMarker.FileAbort);
			throw;
		}

		await SendAsync(TransferMarker.FileEnd);
	}

	private async Task SendAsync(string value, string debugName)
	{
		logger.Log($"Sending string \"{debugName}\"");
		var bytes = Configuration.Encoding.GetBytes(value);
		await SendAsync(TransferMarker.String);
		await SendAsync(bytes.Length, "String length");
		await networkStream.WriteAsync(bytes, cancellationToken);
		logger.Log($"Sent string \"{value}\".");
	}

	private async Task SendAsync(int value, string debugName)
	{
		logger.Log($"Sending int \"{debugName}\"");
		var bytes = BitConverter.GetBytes(value);
		await SendAsync(TransferMarker.Int);
		await networkStream.WriteAsync(bytes, cancellationToken);
		logger.Log($"Sent int {value}.");
	}

	private async Task SendAsync(long value, string debugName)
	{
		logger.Log($"Sending long \"{debugName}\"");
		var bytes = BitConverter.GetBytes(value);
		await SendAsync(TransferMarker.Long);
		await networkStream.WriteAsync(bytes, cancellationToken);
		logger.Log($"Sent long {value}.");
	}

	private async Task SendAsync(TransferMarker marker)
	{
		var bytes = BitConverter.GetBytes((int)marker);
		await networkStream.WriteAsync(bytes, cancellationToken);
		logger.Log($"Sent marker {marker}.");
	}

	private static List<string> GetFilePathsRecursively(string directoryPath)
	{
		return Directory.EnumerateFiles(directoryPath, searchPattern: "*", SearchOption.AllDirectories)
			.ToList();
	}

	private readonly List<string> filePaths = GetFilePathsRecursively(directoryPath);
}
