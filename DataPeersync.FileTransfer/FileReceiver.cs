using System.Net.Sockets;
using DataPeersync.Transfer.Exceptions;

namespace DataPeersync.Transfer;

internal sealed class FileReceiver(
	NetworkStream networkStream,
	string directoryPath,
	ILogger logger,
	CancellationToken cancellationToken)
{
	public async Task<ICollection<string>> ReceiveMultipleAsync()
	{
		logger.Log("Receiving files...");

		await EnsureReceivedMarkerAsync(TransferMarker.Start);

		var fileCount = await ReceiveIntAsync("File count");
		var filePaths = new string[fileCount];

		for (var fileIndex = 0; fileIndex < fileCount; fileIndex++)
		{
			var fileName = await ReceiveStringAsync("File name");
			var filePath = Path.Combine(directoryPath, fileName);
			await ReceiveFileAsync(filePath);
			logger.Log($"Receied file {filePath}.");
			filePaths[fileIndex] = filePath;
		}

		await EnsureReceivedMarkerAsync(TransferMarker.End);

		return filePaths;
	}

	private async Task ReceiveFileAsync(string filePath)
	{
		await EnsureReceivedMarkerAsync(TransferMarker.FileStart);
		CreateDirectoryIfMissing(filePath);
		await using var file = new FileStream(filePath, FileMode.Create);

		var fileSizeInBytes = await ReceiveLongAsync("File size in bytes");

		logger.Log("Receiving file.");
		if (fileSizeInBytes > 0)
		{
			if (fileSizeInBytes > Configuration.ChunkSize)
			{
				var bytesToReceiveNumber = fileSizeInBytes;
				var buffer = new byte[Configuration.ChunkSize].AsMemory();

				do
				{
					if (bytesToReceiveNumber < buffer.Length)
						buffer = buffer[..(int)bytesToReceiveNumber];

					var receivedBytesNumber = await networkStream.ReadAsync(buffer, cancellationToken);
					await file.WriteAsync(buffer[..receivedBytesNumber], cancellationToken);
					bytesToReceiveNumber -= receivedBytesNumber;
				}
				while (bytesToReceiveNumber > 0);

				if (bytesToReceiveNumber < 0)
					throw new UnexpectedDataReceivedException(-(int)bytesToReceiveNumber);
			}
			else
			{
				var buffer = new byte[fileSizeInBytes];
				var receivedBytesNumber = await networkStream.ReadAsync(buffer, cancellationToken);

				if (receivedBytesNumber < buffer.Length)
					throw new InsufficientDataReceivedException(receivedBytesNumber, buffer.Length);

				await file.WriteAsync(buffer[..receivedBytesNumber]);
			}
		}

		await EnsureReceivedMarkerAsync(TransferMarker.FileEnd);
	}

	private async Task<string> ReceiveStringAsync(string debugName)
	{
		logger.Log($"Expects string \"{debugName}\"");
		await EnsureReceivedMarkerAsync(TransferMarker.String);
		var bytes = await ReceiveBytesAsync();
		var value = Configuration.Encoding.GetString(bytes);
		logger.Log($"Got string \"{value}\".");
		return value;
	}

	private async Task<int> ReceiveIntAsync(string debugName)
	{
		logger.Log($"Expects int \"{debugName}\"");
		await EnsureReceivedMarkerAsync(TransferMarker.Int);
		var bytes = await ReceiveBytesAsync(sizeof(int));
		var value = BitConverter.ToInt32(bytes);
		logger.Log($"Received int {value}.");
		return value;
	}

	private async Task<long> ReceiveLongAsync(string debugName)
	{
		logger.Log($"Expects long \"{debugName}\"");
		await EnsureReceivedMarkerAsync(TransferMarker.Long);
		var bytes = await ReceiveBytesAsync(sizeof(long));
		var value = BitConverter.ToInt64(bytes);
		logger.Log($"Received long {value}.");
		return value;
	}

	private async Task<byte[]> ReceiveBytesAsync()
	{
		var bytesNumber = await ReceiveIntAsync("Bytes number");
		var value = await ReceiveBytesAsync(bytesNumber);
		logger.Log($"Received {bytesNumber} bytes.");
		return value;
	}

	private async Task EnsureReceivedMarkerAsync(TransferMarker marker)
	{
		var value = await ReceiveMarkerAsync();

		if (value == marker)
			logger.Log($"Received marker {marker} as expected.");
		else
			throw new InvalidDataException($"Received marker {value} but was expected {marker}.");
	}

	private async Task<TransferMarker> ReceiveMarkerAsync()
	{
		var bytes = await ReceiveBytesAsync(sizeof(int));
		return (TransferMarker)BitConverter.ToInt32(bytes);
	}

	private async Task<byte[]> ReceiveBytesAsync(int bytesNumber)
	{
		var bytes = new byte[bytesNumber];
		await networkStream.ReadAsync(bytes, cancellationToken);

		return bytes;
	}

	private static void CreateDirectoryIfMissing(string filePath)
	{
		var directoryPath = Path.GetDirectoryName(filePath);
		if (directoryPath is not null)
			Directory.CreateDirectory(directoryPath);
	}
}
