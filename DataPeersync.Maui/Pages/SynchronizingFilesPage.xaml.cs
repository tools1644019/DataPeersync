using System.Net;
using CommunityToolkit.Maui.Storage;
using DataPeersync.Persistance;
using DataPeersync.Transfer;

namespace DataPeersync.Maui.Pages;

public partial class SynchronizingFilesPage : ILogger
{
	public SynchronizingFilesPage()
	{
		InitializeComponent();

		connector = new Connector(logger: this);
		fileSynchronizer = new FileSynchronizer(logger: this);

		_ = connector.BroadcastPresenceAsync(GetCancellationToken());
		_ = GetRecipientIpAsync();
		_ = InitializeAsync();
	}

	public void Log(string message)
	{
		LogLabel.Text += message;
		LogLabel.Text += Environment.NewLine;
	}

	private async Task InitializeAsync()
	{
		DirectoryPathLabel.Text = directoryPath = await repository.GetSyncronizationDirectoryPathAsync();
	}

	private async Task GetRecipientIpAsync()
	{
		try
		{
			targetIp = await connector.GetOtherDeviceIpAsync(GetCancellationToken());
			TargetIpLabel.Text = targetIp?.ToString();
			CheckIfSynchronizingPossible();
		}
		catch (OperationCanceledException) { }
	}

	private async void BrowseDirectoryAsync(object sender, EventArgs e)
	{
		if (!await RequestWritePermissionAsync())
			return;

		var result = await FolderPicker.Default.PickAsync();

		if (result?.Folder is not null)
		{
			DirectoryPathLabel.Text = directoryPath = result.Folder.Path;
			await repository.SetSyncronizationDirectoryPathAsync(directoryPath);
		}

		CheckIfSynchronizingPossible();
	}

	private void CheckIfSynchronizingPossible()
	{
		SynchronizeFilesButton.IsEnabled = targetIp is not null
			&& directoryPath is not null;
	}

	private async void SynchronizeFilesAsync(object sender, EventArgs e)
	{
		if (!await RequestWritePermissionAsync())
			return;

		Log("Synchronizing...");

		try
		{
			await fileSynchronizer.SynchronizeAsync(directoryPath!, targetIp!, GetCancellationToken());
			Log("Synchronizing succeed.");
		}
		catch (Exception exception)
		{
			Log($"Synchronizing failed. {exception}");
		}
	}

	private async Task<bool> RequestWritePermissionAsync()
	{
		Log("Requesting permissions...");

		var granted = await Permissions.RequestAsync<Permissions.StorageRead>() is PermissionStatus.Granted
			&& await Permissions.RequestAsync<Permissions.StorageWrite>() is PermissionStatus.Granted;

		if (granted)
			Log("Permissions granted.");
		else
			Log("Permissions not granted.");

		return granted;
	}

	private CancellationToken GetCancellationToken()
	{
		var cancellationTokenSource = new CancellationTokenSource();
		Disappearing += (_, _) => cancellationTokenSource.Cancel();

		return cancellationTokenSource.Token;
	}

	private IPAddress? targetIp;
	private string? directoryPath;
	private readonly Connector connector;
	private readonly FileSynchronizer fileSynchronizer;
	private readonly Repository repository = new(databaseDirectoryPath: FileSystem.AppDataDirectory);
}
