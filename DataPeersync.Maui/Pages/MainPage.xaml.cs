﻿namespace DataPeersync.Maui.Pages;

public partial class MainPage
{
	public MainPage()
	{
		InitializeComponent();

		GoToSynchronizingFilesPageButton.Clicked
			+= async (_, _) => await Navigation.PushAsync(new SynchronizingFilesPage());
	}
}
