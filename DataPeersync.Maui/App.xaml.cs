﻿using DataPeersync.Maui.Pages;

namespace DataPeersync.Maui;

public partial class App : Application
{
	public App()
	{
		InitializeComponent();

		MainPage = new NavigationPage(
			new MainPage());
	}

	protected override Window CreateWindow(IActivationState? activationState)
	{
		var window = base.CreateWindow(activationState);
		window.Title = "Data Peersync";

		return window;
	}
}
